var redis = require('redis');
var redisClient = redis.createClient();

exports.index = function(req, res) {
  res.render('index', { title: 'Express' });
};

exports.submit = function(req, res) {
  var params = req.body;
  var data = {
    latitude: params.latitude,
    longitude: params.longitude,
    time: (new Date()).getTime(),
    symptoms: params.symptoms
  };
  redisClient.zadd("self_report", data.time, JSON.stringify(data));
  res.status(200).send();
};

// exports.maps = function(req, res) {
//   res.render('maps');
// }
