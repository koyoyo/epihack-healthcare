
var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var io = require('socket.io');
var redis = require('redis');
var redisClient = redis.createClient();

var server;
var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.post('/submit', routes.submit);
//app.get('/maps', routes.maps);

server = http.createServer(app);
server.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});

io = io.listen(server);
io.sockets.on('connection', function(socket) {
  setInterval(function() {
    var lastSubmit;
    redisClient.get("last_submit", function(err, data) {
      lastSubmit = parseInt(data);
    });
    redisClient.zrevrange("self_report", 0, 0, function(err, data) {
      var _data = JSON.parse(data);
      if (lastSubmit !== _data.time) {
        redisClient.set("last_submit", _data.time);
        socket.emit('maps', data);
        socket.broadcast.emit('maps', data);
      }
    });
  }, 500);
});
